﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace TrayBox
{
    public static class IconCacheManifest
    {
        
        /// <summary>
        /// Gets the config object
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string,string> Load()
        {
            Dictionary<string,string> config = JsonConvert.DeserializeObject<Dictionary<string,string>>(File.ReadAllText(IconCacheManifestPath));
            return config;
        }

        public static void Save(Dictionary<string,string> ICM)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets filepath that contains link_path to bitmap translation 
        /// </summary>
        public static string IconCacheManifestPath
        {
            get
            {
                string filePath = @"cache\iconcache.json";
                string cachePath = Path.Combine(Program.GetAppDir, filePath);

                if (!File.Exists(cachePath))
                {
                    Directory.CreateDirectory(Path.Combine(Program.GetAppDir, "cache"));
                    var cacheFile = File.CreateText(cachePath);
                    cacheFile.WriteLine("{ }");
                    cacheFile.Close();
                }
                return cachePath;
            }
        }

    }
}
