using NLog;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrayBox
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            SetupLog();
            Program.Log.Info("Program start. Log set.");

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            AppConfig.Configure();
            Application.Run(new Form1());
        }

        static Logger _appWideLog = null;
        public static Logger Log
        {
            get
            {
                return _appWideLog;
            }
        }

        public static void SetupLog()
        {
            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = "TrayBox.log" };
            

            // Rules for mapping loggers to targets                        
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);
            

            // Apply config           
            NLog.LogManager.Configuration = config;
            _appWideLog = NLog.LogManager.GetLogger("TrayBox");
        }

        public static string GetAppDir
        {
            get
            {
                String dir = AppDomain.CurrentDomain.BaseDirectory.ToString();
                return dir;
            }
        }
    }
}
