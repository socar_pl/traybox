﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace TrayBox
{
    public class TrayBoxMenuItem : ToolStripMenuItem
    {
        public bool IsDirectory { get; set; }
    }
}
