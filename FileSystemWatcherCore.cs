﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Timers;

namespace TrayBox
{
    public class FileSystemWatcherCore
    {
        public FileSystemWatcher _rootDirChangeWatcher = null;
        /// <summary>
        /// This encapsulates FileSysstemWatcher and allows to trigger a "general" event that something changed once per defined delay
        /// </summary>
        /// <param name="configPath">Path to monitor</param>
        /// <param name="delayInMiliseconds">Delay in miliseconds between each notification</param>
        public FileSystemWatcherCore(string configPath, int delayInMiliseconds=1500)
        {
            
            _rootDirChangeWatcher = new FileSystemWatcher(AppConfig.Instance.RootDir);
            _rootDirChangeWatcher.IncludeSubdirectories = true;
            _rootDirChangeWatcher.Changed += rootDirChangeWatcher_Changed;
            _rootDirChangeWatcher.Created += rootDirChangeWatcher_Created;
            _rootDirChangeWatcher.Renamed += rootDirChangeWatcher_Renamed;
            _rootDirChangeWatcher.Deleted += rootDirChangeWatcher_Deleted;
            _rootDirChangeWatcher.EnableRaisingEvents = true;

            _refreshTimer = new Timer();
            _refreshTimer.Interval = delayInMiliseconds;
            _refreshTimer.Elapsed += _refreshTimer_Elapsed;          

            
        }

        private void _refreshTimer_Elapsed(object sender, ElapsedEventArgs e)
        {

            System.Diagnostics.Debug.WriteLine("tick");
            _refreshTimer.Stop();

            if (_refreshRequired)
            {
                System.Diagnostics.Debug.WriteLine("REFRESH_REQUIRED=TRUE");
                _refreshRequired = false;
                TriggerRootDirChanged();
            }
            else
                System.Diagnostics.Debug.WriteLine("REFRESH_REQUIRED=FALSE");
        }

        /// <summary>
        /// Object used to lock access to block of code while triggered
        /// </summary>
        private object multithreadlock = new object();
   
        /// <summary>
        /// flag for timer if refresh is required
        /// </summary>
        private bool _refreshRequired = false;

        /// <summary>
        /// timer that is triggered when refresh starts. There are numerous refreshment events but only one should be triggered. 
        /// </summary>
        private Timer _refreshTimer = null;

        private void rootDirChangeWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            _refreshRequired = true;
            TriggerRootDirChanged();
        }

        private void rootDirChangeWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            _refreshRequired = true;
            TriggerRootDirChanged();
        }

        private void rootDirChangeWatcher_Created(object sender, FileSystemEventArgs e)
        {
            _refreshRequired = true;
            TriggerRootDirChanged();
        }

        private void rootDirChangeWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            _refreshRequired = true;
            TriggerRootDirChanged();
        }

        public delegate void RootDirChangedCallback();
        public event RootDirChangedCallback RootDirChanged;


   
        private void TriggerRootDirChanged()
        {
            lock (multithreadlock)
            {
                
                System.Diagnostics.Debug.WriteLine("_refreshTimer.Enabled:"+ _refreshTimer.Enabled.ToString());

                if (_refreshTimer.Enabled)
                    return;

                if (RootDirChanged != null)
                {
                    RootDirChangedCallback rd = RootDirChanged;
                    rd?.Invoke();
                }

                System.Diagnostics.Debug.WriteLine("starting timer");
                _refreshTimer.Start();
            }

        }

        
    }
}
