﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography;
using System.Text;

namespace TrayBox
{
    public static class RunCore
    {
        public static void Run(string filepath)
        {
            var p = new Process();
            p.StartInfo = new ProcessStartInfo(filepath)
            {
                UseShellExecute = true
            };
            try
            {
                p.Start();
            }
            catch (System.ComponentModel.Win32Exception ex){
                if (ex.Message.StartsWith("The operation was canceled by the user")) { /*ignore -- user pressed "No" on UAC dialog box */}
            }
            //this should be enriched to identify LNK with parameters and such 
        }
    }
}
