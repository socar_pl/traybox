﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;
using System.Linq;

namespace TrayBox
{
    public static class UIElementsFactory
    {
        static UIElementsFactory()
        {
            _dummyTSMI.Tag = "DUMMYDIR";
        }
        public static TrayBoxMenuItem GetFileMenuItem(FileInfo itemFI)
        {
            if (File.Exists(itemFI.FullName))
            {
                FileInfo fi = itemFI;
                TrayBoxMenuItem cms = GetGenericMenuItem(ExtractNameFromFileInfo(itemFI));
                cms.Image = IconCache.Instance.GetIcon(fi.FullName);
                cms.Click += MenuItemClicked;
                cms.Tag = fi;
                return cms;

            }
            else
                return null;
        }

        private static string ExtractNameFromFileInfo(FileInfo itemFI)
        {
            string returnedValue = "N/A";

            if(itemFI!=null)
            {
                if (itemFI.Extension.Length > 0)
                {
                    returnedValue = itemFI.Name.Remove(itemFI.Name.Length - itemFI.Extension.Length, itemFI.Extension.Length);
                }
                else
                    returnedValue = itemFI.Name;
            }

            return returnedValue;
        
        }

        private static TrayBoxMenuItem GetGenericMenuItem(String Name)
        {
            TrayBoxMenuItem cms = new TrayBoxMenuItem();
            cms.Name = String.Format("_{0}_TrayBoxMenuItem_{1}", UIElementsFactory.TicketNumber, Name);
            cms.Text = Name;
            return cms;
        }
        

        private static void MenuItemClicked(object sender, EventArgs e)
        {
            if (sender != null && sender is TrayBoxMenuItem)
            {
                TrayBoxMenuItem s = sender as TrayBoxMenuItem;
                if (s.Tag != null && s.Tag is FileInfo)
                {
                    FileInfo f = s.Tag as FileInfo;
                    RunCore.Run(f.FullName);
                }
            }
        }

        public static List<string> GetAllFilePathRecursively(string path)
        {
            List<string> returned = new List<string>();

            if(Directory.Exists(path))
            {
                DirectoryInfo di = new DirectoryInfo(path);
                var filePaths = from file in di.GetFiles()
                                select file.FullName;
                returned.AddRange(filePaths.ToList());

                foreach(var directory in di.GetDirectories())
                {
                    returned.AddRange(GetAllFilePathRecursively(directory.FullName));
                }

            }

            return returned;
        }

        public static List<TrayBoxMenuItem> GetDirectoryStructure(string path, bool useLazyLoading = true)
        {
            //check if dir exist
            return GetDirectoryStructure(new DirectoryInfo(path), useLazyLoading);
        }
        public static List<TrayBoxMenuItem> GetDirectoryStructure(DirectoryInfo di, bool useLazyLoading = true)
        {
            TrayBoxMenuItem retFolder = new TrayBoxMenuItem();

            List<TrayBoxMenuItem> children = new List<TrayBoxMenuItem>();
            foreach (var file in di.GetFiles())
            {
                if (isValidFile(file))
                {
                    var tsmi = GetFileMenuItem(file);
                    children.Add(tsmi);
                }
            }

            foreach (var dir in di.GetDirectories())
            {
                var tsmi = GetDirectoryMenuItem(dir, useLazyLoading);
                children.Add(tsmi);
            }

            return children;

        }

        private static bool isValidFile(FileInfo file)
        {
            if (AppConfig.Instance.IgnoredExtensions.Contains(file.Extension) || AppConfig.Instance.IgnoredFiles.Contains(file.Name))
                return false;

            return true;
        }

        public static TrayBoxMenuItem GetDirectoryMenuItem(DirectoryInfo dir, bool useLazyLoading = true)
        {
            TrayBoxMenuItem tsmi = GetGenericMenuItem(dir.Name);
            tsmi.Image = TrayBox.Properties.Resources.Folder_icon;
            tsmi.IsDirectory = true;
            tsmi.Tag = dir;

            if (useLazyLoading)
            {
                tsmi.DropDownItems.Clear();
                tsmi.DropDownItems.AddRange(new ToolStripItem[] { DummyTrayBoxMenuItem });
                tsmi.MouseEnter += Tsmi_MouseEnter;
            }
            else
            {
                tsmi.DropDownItems.AddRange(GetDirectoryStructure(dir).ToArray());                
            }
            
            return tsmi;

        }

        private static void Tsmi_MouseEnter(object sender, EventArgs e)
        {
            if (sender != null && sender is TrayBoxMenuItem)
            {
                TrayBoxMenuItem tbmi = sender as TrayBoxMenuItem;
                if (tbmi != null && tbmi.Tag != null)
                {

                    //Check if this dir have structure already.
                    if (HasDummyElement(tbmi))
                    {
                        DirectoryInfo dir = tbmi.Tag as DirectoryInfo;
                        if (dir != null)
                        {
                            System.Diagnostics.Debug.WriteLine("Loading dir: " + dir.FullName);
                            //DirectoryInfo at this point
                            var dirStructure = GetDirectoryStructure(dir);
                            tbmi.DropDownItems.Clear();
                            tbmi.DropDownItems.AddRange(dirStructure.ToArray());
                        }
                    }
                   
                }
                
            }
        }

        private static bool HasDummyElement(TrayBoxMenuItem tbmi)
        {
            if (tbmi.DropDownItems.Count == 0)
                return true;

            var firstitem = tbmi.DropDownItems[0];
            if (firstitem.Tag != null && firstitem.Tag.ToString() == "DUMMYDIR")
                return true;

            return false;
        }

        public static TrayBoxMenuItem _dummyTSMI = new TrayBoxMenuItem();
        /// <summary>
        /// Gets dummy TrayBoxMenuItem that is used to show a "arrow" on folder icons, while not loading their content
        /// Content of a menu entry is loaded after mouse is hovered over it
        /// </summary>
        public static TrayBoxMenuItem DummyTrayBoxMenuItem
        {
            get { return _dummyTSMI; }
        }


        public static object lockObject = new object();


        public static uint ticketNumber = 0;

        /// <summary>
        /// this is to ensure each element will have it's unique name, without complicated timestamping, guid generation etc. 
        /// this gives you 4,294,967,295 unique elements with 
        /// </summary>
        public static uint TicketNumber
        {
            get
            {
                return ticketNumber++;
            }
        }
    }
}
