﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace TrayBox
{
    public class AppConfig
    {
        public static AppConfig _mainInstance = null;
        public static AppConfig Instance
        {
            get
            {
                if (_mainInstance == null)
                    _mainInstance = new AppConfig();

                return _mainInstance;
            }
            set
            {
                _mainInstance = value;
            }
        }

        internal static void Configure()
        {
            AppConfig.Instance = AppConfig.LoadConfig();
        }

        private string _rootDir = @"D:\Database\ThisComputer\TrayBox";

        public static String TrayBoxConfigPath
        {
            get
            {
                string filename = "TrayBoxConfig.json";
                string path = Path.Combine(Program.GetAppDir, filename);
                return path;
            }
        }
        public static AppConfig LoadConfig()
        {
            return LoadConfig(AppConfig.TrayBoxConfigPath);
        }
        public static AppConfig LoadConfig(string path)
        {
            AppConfig config = JsonConvert.DeserializeObject<AppConfig>(File.ReadAllText(path));

            return config;           
        }

        public static void SaveConfig(AppConfig config)
        {
            SaveConfig(config, AppConfig.TrayBoxConfigPath);
        }

        public static void SaveConfig(AppConfig config, string configPath)
        {
            if (File.Exists(configPath))
            {
                File.Copy(configPath, configPath + ".bak",true);
                File.Delete(configPath);
            }

            string text = JsonConvert.SerializeObject(config, Formatting.Indented);
            using (var fileToWrite = File.CreateText(AppConfig.TrayBoxConfigPath))
            {
                fileToWrite.WriteLine(text);
                fileToWrite.Flush();
                fileToWrite.Close();
            }
        }


        public string RootDir
        {
            get
            {
                return _rootDir;
            }

            set
            {
                _rootDir = value;
            }
        }

        private bool _useLazyLoading = true;
        
        public List<string> IgnoredExtensions { get; set; }
        public List<string> IgnoredFiles { get; set; }
        public bool UseLazyLoading { get => _useLazyLoading; set => _useLazyLoading = value; }
    }
}
