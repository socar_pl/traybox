﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrayBox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Form1.Instance = this;
        }




        private static Form1 instance = null;
        FileSystemWatcherCore _fswc = null;

        private void Form1_Load(object sender, EventArgs e)
        {
            _fswc = new FileSystemWatcherCore(AppConfig.Instance.RootDir);
            _text_RootPath.Text = AppConfig.Instance.RootDir;
            _fswc.RootDirChanged += _fswc_RootDirChanged;
            
            RefreshMenuUI();
        }

        private void _fswc_RootDirChanged()
        {
            System.Diagnostics.Debug.WriteLine("_fswc_RootDirChanged");
            RefreshMenuUI();
        }

        public delegate void RefreshMenuUICallback();
        public void RefreshMenuUI()
        {            
            if (_contextMenu_AppMenu.InvokeRequired)
            {
                _contextMenu_AppMenu.Invoke(new RefreshMenuUICallback(RefreshMenuUI));
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("refreshing");
                List<TrayBoxMenuItem> linksDirectoryStructure = UIElementsFactory.GetDirectoryStructure(AppConfig.Instance.RootDir, false);
                _contextMenu_AppMenu.SuspendLayout();
                System.Diagnostics.Debug.WriteLine("element count: " + linksDirectoryStructure.Count());

                _contextMenu_AppMenu.Items.Clear();
                _contextMenu_AppMenu.Items.AddRange(linksDirectoryStructure.ToArray());

                _contextMenu_AppMenu.ResumeLayout();
                _contextMenu_AppMenu.Invalidate();
                _contextMenu_AppMenu.Refresh();
            }
        }

        private static FileSystemWatcher _rootDirChangeWatcher = null;

        public static FileSystemWatcher RootDirChangeWatcher { get => _rootDirChangeWatcher; set => _rootDirChangeWatcher = value; }
        public static Form1 Instance { get => instance; set => instance = value; }

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshMenuUI();
            MessageBox.Show("Done!", "Menu refresh", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        private void _tray_Icon_MouseClick(object sender, MouseEventArgs e)
        {


            if (e.Button == MouseButtons.Left)
            {
                ReplaceMenuIfShiftPressed();

                MethodInfo mi = typeof(NotifyIcon).GetMethod("ShowContextMenu", BindingFlags.Instance | BindingFlags.NonPublic);
                mi.Invoke(_tray_Icon, null);
            }


        }

        bool wasShiftPressed = false;
        private void _tray_Icon_MouseDown(object sender, MouseEventArgs e)
        {
            ReplaceMenuIfShiftPressed();
        }

        public void ReplaceMenuIfShiftPressed()
        {
            if (Control.ModifierKeys == Keys.Shift)
            {

                wasShiftPressed = true;
                _tray_Icon.ContextMenuStrip = _contextMenu_SettingsMenu;

            }
        }

        private void _button_OpenRootFolder_Click(object sender, EventArgs e)
        {
            
        }

        private void _tray_Icon_MouseUp(object sender, MouseEventArgs e)
        {
            if (wasShiftPressed)
            {
                _tray_Icon.ContextMenuStrip = _contextMenu_AppMenu;
                wasShiftPressed = false;
            }
        }

        private void _button_SelectDir_Click(object sender, EventArgs e)
        {
            if(folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                _text_RootPath.Text = folderBrowserDialog1.SelectedPath;
                AppConfig.Instance.RootDir = _text_RootPath.Text;
                AppConfig.SaveConfig(AppConfig.Instance);
                RefreshMenuUI();
            }
        }

        private void _button_ExploreDir_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", AppConfig.Instance.RootDir);
        }

        private void _checkbox_LazyLoading_CheckedChanged(object sender, EventArgs e)
        {
            AppConfig.Instance.UseLazyLoading = _checkbox_LazyLoading.Checked;
            AppConfig.SaveConfig(AppConfig.Instance);
            RefreshMenuUI();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1.Instance.Show();
            Form1.Instance.TopMost = true;
            Form1.Instance.BringToFront();
            Form1.Instance.TopMost = false;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void _button_About_Click(object sender, EventArgs e)
        {
            MessageBox.Show("(c) SOCAR 2021\r\nTrayBox\r\nSimple tool to translate folder structure into usable shrotcuts in context menu of tray icon\r\n\r\nhttps://www.socar.pl\r\n\r\nver 1.0", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
