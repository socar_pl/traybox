﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TrayBox
{
    public class TrayBoxCore
    {
        public List<DirectoryInfo> GetFoldersFromRootDir()
        {
            return GetFoldersFromDir(AppConfig.Instance.RootDir);
        }
        
        public List<DirectoryInfo> GetFoldersFromDir(string dir)
        {
            if (Directory.Exists(dir))
            {
                DirectoryInfo di = new DirectoryInfo(dir);
                return di.GetDirectories().ToList();
            }
            else
                return new List<DirectoryInfo>();
        }

        public List<FileInfo> GetFilesFromDir(String dir, string searchPattern = "")
        {
            DirectoryInfo di = new DirectoryInfo(dir);
            return di.GetFiles(searchPattern).ToList(); 
        }

        public List<FileInfo> GetFilesFromRootDir()
        {
            return GetFilesFromDir(AppConfig.Instance.RootDir);
        }
    }
}
