﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace TrayBox
{
    public class IconCache
    {
        /// <summary>
        /// holds cache of icons, loaded from file and updated during programm operation
        /// </summary>
        Dictionary<string, System.Drawing.Image> _iconCache = new Dictionary<string, Image>();

        public IconCache()
        {
           
        }

        public static IconCache _mainInstance = null;
        public static IconCache Instance
        {
            get
            {
                if (_mainInstance == null)
                    _mainInstance = new IconCache();

                return _mainInstance;
            }
        }

        public void Initialize()
        {
            Program.Log.Info("Initializing cache");
            Dictionary<string, string> ICM = IconCacheManifest.Load();
            this._iconCache = ConstructCache(ICM);

        }

        /// <summary>
        /// Constructs Cache From file
        /// </summary>
        /// <param name="ICM"></param>
        /// <returns></returns>
        private Dictionary<string, Image> ConstructCache(Dictionary<string, string> ICM)
        {
            Dictionary<string, Image> tmpCache = new Dictionary<string, Image>();
            Program.Log.Info("Constructing Icon Cache");
            if (ICM != null && ICM.Count > 0)
            {
                foreach (var dicItem in ICM)
                {
                    tmpCache.Add(dicItem.Key.ToLower(), Bitmap.FromFile(dicItem.Value));
                }
                Program.Log.Info($"Processed {ICM.Count} strings");
            }
            else
                Program.Log.Info("ICM is null or have no entries");

            return tmpCache;
        }

       


        public Image GetIcon(string filepath)
        {
            if (!String.IsNullOrEmpty(filepath))
            {
                string filepathL = filepath.ToLower();
                if (_iconCache.ContainsKey(filepathL))
                    return _iconCache[filepathL];
                else
                {
                    
                    Icon ic = ResourceExtractor.GetIcon(filepathL, true);
                    Image i = ic.ToBitmap();
                    return i;
                }

            }
            else
                return null;
   
        }

    

        
    }
}
