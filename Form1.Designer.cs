﻿namespace TrayBox
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this._tray_Icon = new System.Windows.Forms.NotifyIcon(this.components);
            this._contextMenu_AppMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this._button_RefreshStructure = new System.Windows.Forms.Button();
            this._contextMenu_SettingsMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._text_RootPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._button_ExploreDir = new System.Windows.Forms.Button();
            this._button_SelectDir = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this._checkbox_LazyLoading = new System.Windows.Forms.CheckBox();
            this._button_About = new System.Windows.Forms.Button();
            this._contextMenu_AppMenu.SuspendLayout();
            this._contextMenu_SettingsMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tray_Icon
            // 
            this._tray_Icon.ContextMenuStrip = this._contextMenu_AppMenu;
            this._tray_Icon.Icon = ((System.Drawing.Icon)(resources.GetObject("_tray_Icon.Icon")));
            this._tray_Icon.Text = "TrayBox";
            this._tray_Icon.Visible = true;
            this._tray_Icon.MouseClick += new System.Windows.Forms.MouseEventHandler(this._tray_Icon_MouseClick);
            this._tray_Icon.MouseDown += new System.Windows.Forms.MouseEventHandler(this._tray_Icon_MouseDown);
            this._tray_Icon.MouseUp += new System.Windows.Forms.MouseEventHandler(this._tray_Icon_MouseUp);
            // 
            // _contextMenu_AppMenu
            // 
            this._contextMenu_AppMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this._contextMenu_AppMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem3});
            this._contextMenu_AppMenu.Name = "rootMenu";
            this._contextMenu_AppMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this._contextMenu_AppMenu.Size = new System.Drawing.Size(189, 52);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(188, 24);
            this.toolStripMenuItem1.Text = "menuMenuItem";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(218, 26);
            this.toolStripMenuItem2.Text = "dzieckoMenuItema";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(188, 24);
            this.toolStripMenuItem3.Text = "zwyklyMenuItem";
            // 
            // _button_RefreshStructure
            // 
            this._button_RefreshStructure.Location = new System.Drawing.Point(407, 64);
            this._button_RefreshStructure.Name = "_button_RefreshStructure";
            this._button_RefreshStructure.Size = new System.Drawing.Size(189, 29);
            this._button_RefreshStructure.TabIndex = 1;
            this._button_RefreshStructure.Text = "Manual Menu Refresh";
            this._button_RefreshStructure.UseVisualStyleBackColor = true;
            this._button_RefreshStructure.Click += new System.EventHandler(this.button1_Click);
            // 
            // _contextMenu_SettingsMenu
            // 
            this._contextMenu_SettingsMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this._contextMenu_SettingsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this._contextMenu_SettingsMenu.Name = "_contextMenu_SettingsMenu";
            this._contextMenu_SettingsMenu.Size = new System.Drawing.Size(132, 52);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(131, 24);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(131, 24);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // _text_RootPath
            // 
            this._text_RootPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._text_RootPath.Location = new System.Drawing.Point(111, 19);
            this._text_RootPath.Name = "_text_RootPath";
            this._text_RootPath.ReadOnly = true;
            this._text_RootPath.Size = new System.Drawing.Size(727, 27);
            this._text_RootPath.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Menu folder:";
            // 
            // _button_ExploreDir
            // 
            this._button_ExploreDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._button_ExploreDir.Image = global::TrayBox.Properties.Resources.Folder_icon1;
            this._button_ExploreDir.Location = new System.Drawing.Point(886, 17);
            this._button_ExploreDir.Name = "_button_ExploreDir";
            this._button_ExploreDir.Size = new System.Drawing.Size(32, 29);
            this._button_ExploreDir.TabIndex = 5;
            this._button_ExploreDir.UseVisualStyleBackColor = true;
            this._button_ExploreDir.Click += new System.EventHandler(this._button_ExploreDir_Click);
            // 
            // _button_SelectDir
            // 
            this._button_SelectDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._button_SelectDir.Image = global::TrayBox.Properties.Resources.Folder_Explorer_icon;
            this._button_SelectDir.Location = new System.Drawing.Point(844, 17);
            this._button_SelectDir.Name = "_button_SelectDir";
            this._button_SelectDir.Size = new System.Drawing.Size(36, 29);
            this._button_SelectDir.TabIndex = 6;
            this._button_SelectDir.UseVisualStyleBackColor = true;
            this._button_SelectDir.Click += new System.EventHandler(this._button_SelectDir_Click);
            // 
            // _checkbox_LazyLoading
            // 
            this._checkbox_LazyLoading.AutoSize = true;
            this._checkbox_LazyLoading.Location = new System.Drawing.Point(111, 69);
            this._checkbox_LazyLoading.Name = "_checkbox_LazyLoading";
            this._checkbox_LazyLoading.Size = new System.Drawing.Size(118, 24);
            this._checkbox_LazyLoading.TabIndex = 7;
            this._checkbox_LazyLoading.Text = "Lazy Loading";
            this._checkbox_LazyLoading.UseVisualStyleBackColor = true;
            this._checkbox_LazyLoading.CheckedChanged += new System.EventHandler(this._checkbox_LazyLoading_CheckedChanged);
            // 
            // _button_About
            // 
            this._button_About.Location = new System.Drawing.Point(886, 64);
            this._button_About.Name = "_button_About";
            this._button_About.Size = new System.Drawing.Size(32, 29);
            this._button_About.TabIndex = 8;
            this._button_About.Text = "?";
            this._button_About.UseVisualStyleBackColor = true;
            this._button_About.Click += new System.EventHandler(this._button_About_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 111);
            this.Controls.Add(this._button_About);
            this.Controls.Add(this._checkbox_LazyLoading);
            this.Controls.Add(this._button_SelectDir);
            this.Controls.Add(this._button_ExploreDir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._text_RootPath);
            this.Controls.Add(this._button_RefreshStructure);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "TrayBox: Settings";
            this.Load += new System.EventHandler(this.Form1_Load);
            this._contextMenu_AppMenu.ResumeLayout(false);
            this._contextMenu_SettingsMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon _tray_Icon;
        private System.Windows.Forms.ContextMenuStrip _contextMenu_AppMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.Button _button_RefreshStructure;
        private System.Windows.Forms.ContextMenuStrip _contextMenu_SettingsMenu;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TextBox _text_RootPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _button_ExploreDir;
        private System.Windows.Forms.Button _button_SelectDir;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox _checkbox_LazyLoading;
        private System.Windows.Forms.Button _button_About;
    }
}

